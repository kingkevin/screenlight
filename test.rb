#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 1.9
require 'serialport'

screenlight = SerialPort.open("/dev/ttyUSB0",{ baud: 115200, databits: 8, parity: SerialPort::NONE, stop_bit: 1, flow_control: SerialPort::NONE})
NB_LEDS = 60

# reset arduino
screenlight.dtr = 1
sleep 0.5
screenlight.dtr = 0
sleep 0.5
screenlight.flush_output
screenlight.flush_input
screenlight.baud = 115200
  
puts "waiting for welcome message"
puts screenlight.gets

puts "moving dot"
NB_LEDS.times do |led|
  msg = [0xff,0x00,0x00,3*NB_LEDS]+[0x00]*3*NB_LEDS
  msg[4+led*3] = 0xff
  screenlight.write msg.pack("C*") # set LED to red
  screenlight.flush_output
  sleep 0.1
end

puts "nice colors"
phase = 0
msg = [0xff,0x00,0x00,3*NB_LEDS]+[0x00]*3*NB_LEDS
led_num = [10,9,8,7,6,5,4,3,2,1,0,11,12,13,14,15,16,17,18]+[29,28,27,26,25,24,23,22,21,20,19]+[41,42,43,44,45,46,47,48,40,39,38,37,36,35,34,33,32,31,30].reverse+[59,58,57,56,55,54,53,52,51,50,49].reverse
while true do
  NB_LEDS.times do |i|
    led = led_num[i]
    msg[led*3+4] = (Math::sin(phase*0.9+i*0.2)+1)*128
    msg[led*3+5] = (Math::sin(phase+Math::PI/2+i*0.2)+1)*128
    msg[led*3+6] = (Math::sin(phase*1.1+Math::PI+i*0.2)+1)*128
  end
  screenlight.write msg.pack("C*")
  screenlight.flush_output 
  phase += 0.025
  sleep 0.02
end
