/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h> // Standard Integer Types
#include <stdio.h> // Standard IO facilities
#include <stdlib.h> // General utilities
#include <stdbool.h> // Boolean
#include <string.h> // Strings

#include <avr/io.h> // AVR device-specific IO definitions
#include <util/delay.h> // Convenience functions for busy-wait delay loops
#include <avr/interrupt.h> // Interrupts
#include <avr/wdt.h> // Watchdog timer handling
#include <avr/pgmspace.h> // Program Space Utilities
#include <avr/sleep.h> // Power Management and Sleep Modes

#include "main.h" // main definitions
#include "uart.h" // basic UART functions
#include "ws2812b.h" // to control WS2812B LEDs

/* global variables */
volatile uint8_t uart_in[4]; // input from USART, enough space for RGB values
volatile uint8_t uart_in_i = 0; // UART input index
const uint8_t channels[] = {11,8,11,11,8,11}; // the number of LEDs for each channel

/* flags, set in the interrupts and handled in the main program */
volatile bool uart_flag = false; // data on UART has been received

/* switch off LED */
void led_off(void)
{
	LED_PORT &= ~(1<<LED_IO); // remove power to LED
}

/* switch on LED */
void led_on(void)
{
	LED_PORT |= (1<<LED_IO); // provide power to LED
}

/* toggle LED */
void led_toggle(void)
{
	LED_PIN |= (1<<LED_IO);
}

/* disable watchdog when booting */
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
void wdt_init(void)
{
	MCUSR = 0;
	wdt_disable();

	return;
}

/* receive UART input */
ISR(USART_RX_vect) { /* UART receive interrupt */
	if (uart_in_i<sizeof(uart_in)) {
		uart_in[uart_in_i] = getchar(); // save current character
		uart_in_i++; // got to next one
		uart_flag = true; // warm main programm
	}
}

/* initialize GPIO */
void io_init(void)
{
	/* use UART as terminal */
	uart_init();
	stdout = &uart_output;
	stdin  = &uart_input;

	/* gpio */
	/* LED */
	LED_DDR |= (1<<LED_IO); // LED is driven by pin (set as output)
	led_off();
	
	sei(); /* enable interrupts */
	
	/* WS2812B LEDs */
	ws2812b_init();
	ws2812b_off();
	ws2812b_show();
}

int main(void)
{
	bool header = false; // has the atmolight message header been received
	uint8_t channel = 0; // the current channel
	uint8_t channel_led = 0; // the current LED on the current channel
	
	io_init(); // initialize IOs
	
	led_on();
	printf(PSTR("welcome to the CuVoodoo ScreenLight\n"));
	led_off();
	
	while (true) { // endless loop for micro-controller
		while (uart_flag) {
			uart_flag = false;
			if (!header) { // wait until atmolight header
				if (((uart_in_i==1) && !(uart_in[0]==0xff)) ||
				    ((uart_in_i==2) && !(uart_in[1]==0x00)) ||
				    ((uart_in_i==3) && !(uart_in[2]==0x00)) ||
				    ((uart_in_i==4) && !(uart_in[3]==3*60))) { // verify atmolight header
					led_on(); // indicate something is wrong
					uart_in_i = 0; // reset if start sequence is not detected
				} else if (uart_in_i==4) { // atmolight header received
					led_off(); // remove error indication
					header = true; // remember header has been received
					uart_in_i = 0; // reset to get values
				}
			} else { // receiving color values
				if (uart_in_i>=3) { // RGB color values received
					ws2812b_set_led_color(channel,channel_led,uart_in[0],uart_in[1],uart_in[2]); // set LED color
					channel_led++; // go the next LED
					if (channel_led>=channels[channel]) { // all LEDs of channel have been set
						channel++; // got to next channel
						channel_led = 0; // restart from first LED
					}
					if (channel>=sizeof(channels)) { // all LEDs have been set
						channel = 0; // restart from first channel
						header = false; // wait for header
						ws2812b_show(); // show the new colors
					}
					uart_in_i = 0; // get next color
				}
			}
		}
	}
	return 0;
}

