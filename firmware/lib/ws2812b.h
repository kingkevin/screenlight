/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This library allows to control WS2812B LEDs
 * it uses timer 0 with interruts
 */

/* the port on which the WS2812B LEDs are attached */
#define WS2812B_PORT PORTD
#define WS2812B_DDR DDRD
#define WS2812B_PIN PIND
/* the pin on the previous port on which the WS2812B LEDs are attached
 * use as many strips in parallel on the available pins to save time and memory */
#define WS2812B_MASK 0xfc
#define WS2812B_NB_CHANNELS 6
/* the maximum number of chained WS2812B LEDs attached on a pin
 * use as few in series and prefer putting them in parrallel to save time and memory */
#define WS2812B_NB_LEDS 11

/* initialize port */
bool ws2812b_init(void);
/* set the color of the LED on a channel LED chain, but do not show yet */
bool ws2812b_set_led_color(uint8_t channel, uint8_t led, uint8_t red, uint8_t green, uint8_t blue);
/* switch off all LEDs, but do not show yet */
void ws2812b_off(void);
/* send the RGB values to the LEDs */
void ws2812b_show(void);
