#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 1.9

puts "[global]
interface	127.0.0.1
port		19333
 
[device]
name		screenlight
type		atmo
output		/dev/ttyUSB0
rate		115200
channels	180
interval	20000
 
[color]
name		red
rgb			FF0000
gamma		1.0
adjust		1.0
blacklevel	0.0
 
[color]
name		green
rgb			00FF00
gamma		1.0
adjust		1.0
blacklevel	0.0
 
[color]
name		blue
rgb			0000FF
gamma		1.0
adjust		1.0
blacklevel	0.0
"

# top
screen_start = 0 # where the LED strip starts on the screen (useful when having multiple screens)
screen_end = 100 # where the LED strip end on the screen (useful when having multiple screens)
led_order = [10,9,8,7,6,5,4,3,2,1,0,11,12,13,14,15,16,17,18].reverse # the order of the LED from start to end
led_order.size.times do |i|
  puts "
[light]
name		top#{i.to_s.rjust(2,'0')}-led#{led_order[i].to_s.rjust(2,'0')}
color		red 	screenlight #{led_order[i]*3+1}
color		green 	screenlight #{led_order[i]*3+2}
color		blue 	screenlight #{led_order[i]*3+3}
hscan		#{(screen_start+(screen_end-screen_start)/led_order.size*i).round(2)} #{(screen_start+(screen_end-screen_start)/led_order.size*(i+1)).round(2)}
vscan		0 20"
end

# bottom
screen_start = 0 # where the LED strip starts on the screen (useful when having multiple screens)
screen_end = 100 # where the LED strip end on the screen (useful when having multiple screens)
led_order = [41,42,43,44,45,46,47,48,40,39,38,37,36,35,34,33,32,31,30].reverse # the order of the LED from start to end
led_order.size.times do |i|
  puts "
[light]
name		bottom#{i.to_s.rjust(2,'0')}-led#{led_order[i].to_s.rjust(2,'0')}
color		red 	screenlight #{led_order[i]*3+1}
color		green 	screenlight #{led_order[i]*3+2}
color		blue 	screenlight #{led_order[i]*3+3}
hscan		#{(screen_start+(screen_end-screen_start)/led_order.size*i).round(2)} #{(screen_start+(screen_end-screen_start)/led_order.size*(i+1)).round(2)}
vscan		80 100"
end

# left
screen_start = 0 # where the LED strip starts on the screen (useful when having multiple screens)
screen_end = 100 # where the LED strip end on the screen (useful when having multiple screens)
led_order = [29,28,27,26,25,24,23,22,21,20,19] # the order of the LED from start to end
led_order.size.times do |i|
  puts "
[light]
name		left#{i.to_s.rjust(2,'0')}-led#{led_order[i].to_s.rjust(2,'0')}
color		red 	screenlight #{led_order[i]*3+1}
color		green 	screenlight #{led_order[i]*3+2}
color		blue 	screenlight #{led_order[i]*3+3}
hscan		0 20
vscan		#{(screen_start+(screen_end-screen_start)/led_order.size*i).round(2)} #{(screen_start+(screen_end-screen_start)/led_order.size*(i+1)).round(2)}"
end

# right
screen_start = 0 # where the LED strip starts on the screen (useful when having multiple screens)
screen_end = 100 # where the LED strip end on the screen (useful when having multiple screens)
led_order = [59,58,57,56,55,54,53,52,51,50,49] # the order of the LED from start to end
led_order.size.times do |i|
  puts "
[light]
name		right#{i.to_s.rjust(2,'0')}-led#{led_order[i].to_s.rjust(2,'0')}
color		red 	screenlight #{led_order[i]*3+1}
color		green 	screenlight #{led_order[i]*3+2}
color		blue 	screenlight #{led_order[i]*3+3}
hscan		80 100
vscan		#{(screen_start+(screen_end-screen_start)/led_order.size*i).round(2)} #{(screen_start+(screen_end-screen_start)/led_order.size*(i+1)).round(2)}"
end
